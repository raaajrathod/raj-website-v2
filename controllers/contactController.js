// const AppError = require('../utils/appError');
const sgMail = require('@sendgrid/mail');

// const pug = require('pug');
const htmlToText = require('html-to-text');
// const Email = require('../utils/email');

const catchAsync = require('../utils/catchAsync');

exports.contactMe = catchAsync(async (req, res, next) => {
  const { firstName, subject, email, message } = req.body;

  sgMail.setApiKey(process.env.SENDGRID_PASSWORD);

  const html = `<p>Hello Raj Rathod,</p>
    <p>I am ${firstName}</p>
    <p>My Email : ${email} </p>
    <p>${message}</p>`;

  const mailOptions = {
    from: 'raaajrathod@gmail.com',
    to: 'rajrathod8712@gmail.com',
    subject: `${subject}`,
    html,
    text: htmlToText.fromString(html)
  };

  await sgMail.send(mailOptions);
  res.status(200).json({ status: 'success', response: { msg: 'Email Sent' } });
});
