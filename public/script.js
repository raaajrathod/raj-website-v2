/* eslint-disable radix */
/* eslint-disable no-unneeded-ternary */
/* eslint-disable prefer-const */
/* eslint-disable no-undef */
/* eslint-disable no-use-before-define */
/* eslint-disable prettier/prettier */
$(function () {
    console.log("Document Loaded");

    initalizeRadialChart();
    initializeBrowserHistory();

    window.onhashchange = function (e) {
        let newHash = getHash(e.newURL);
        let oldHash = getHash(e.oldURL);

        displayHashContent(oldHash, newHash);
        $(".mobile-navigation").slideUp("slow");
    };

    particlesJS.load(
        "particles-js",
        "vendor/assets/particlesjs-config.json",
        function () {
            console.log("callback - particles.js config loaded");
        }
    );

    $(".mobile_nav_btn").click(function () {
        $(".mobile-navigation").slideToggle("slow");
    });

    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-left",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }


});

const initialData = {
    currentHash: "#home"
};

function getHash(str) {
    let parts = str.split("#");
    if (parts.length > 1) {
        return `#${parts.pop()}`;
    }

    return null;
}

function displayHashContent(oldHash, newHash) {
    oldHash = oldHash ? oldHash : "#home";
    newHash = newHash ? newHash : "#home";

    $(`${oldHash}`).fadeOut("default", function () {
        $(`${newHash}`).fadeIn("default", function () {
            // console.log(hash);
            initialData.currentHash = newHash;
            initialData.oldHash = oldHash;

            let id = newHash.split("#")[1];
            $(".navlist .list-item a, .mobile-links a").removeClass("active");
            $(`#nav_${id}`).addClass("active");
            $(`#mobile_nav_${id}`).addClass("active");
        });
    });
}

function initializeBrowserHistory() {
    let urlHash = window.location.hash || "";

    let pages = {
        "#home": "home",
        "#resume": "resume",
        "#contact": "contact",
        "#portfolio": "portfolio",
        "#blog": "blog"
    };

    if (pages[urlHash]) {
        window.history.pushState(urlHash, pages[urlHash], urlHash);
    } else {
        urlHash = "#home";
    }
    initialData.currentHash = urlHash;
    displayHashContent("", urlHash);

    // initializePopState();
}

// function initializePopState() {
//     window.onpopstate = function (e) {
//         console.log(e);
//         if (e.state) {
//             console.log(e);
//         }
//     };
// }

function initalizeRadialChart() {
    let options = {
        series: [0],
        chart: {
            height: 200,
            type: "radialBar",
            toolbar: {
                show: false
            }
        },
        plotOptions: {
            radialBar: {
                startAngle: 0,
                endAngle: 360,
                hollow: {
                    margin: 0,
                    size: "80%",
                    background: "transparent",
                    image: undefined,
                    imageOffsetX: 0,
                    imageOffsetY: 0,
                    position: "front",
                    dropShadow: {
                        enabled: true,
                        top: 3,
                        left: 0,
                        blur: 4,
                        opacity: 0.2
                    }
                },
                track: {
                    show: false,
                    background: "#fff",
                    strokeWidth: "30%",
                    margin: 0, // margin is in pixels
                    dropShadow: {
                        enabled: true,
                        top: 0,
                        left: 0,
                        blur: 0,
                        opacity: 1
                    }
                },

                dataLabels: {
                    show: true,
                    name: {
                        offsetY: 10,
                        show: true,
                        color: "#888",
                        fontSize: "2rem"
                    },
                    value: {
                        formatter: function (val) {
                            return parseInt(val);
                        },
                        offsetY: -10,
                        color: "#111",
                        fontSize: "3rem",
                        show: false
                    }
                }
            }
        },
        fill: {
            type: "solid",
            colors: ["#00b4d9"],
            gradient: {
                shade: "dark",
                type: "horizontal",
                shadeIntensity: 0.5,
                gradientToColors: ["#eee"],
                inverseColors: false,
                opacityFrom: 2,
                opacityTo: 1,
                stops: [0, 100]
            }
        },
        stroke: {
            lineCap: "round",
            width: 0.2
        },
        labels: ["Good"]
    };

    // Create your event handlers
    function showChart(e) {
        renderRadialChart(options, "leadership");
        renderRadialChart(options, "work_time");
        renderRadialChart(options, "problem_solving");
    }

    function removeChart(e) {
        $(".skill-chart").empty();
    }
    // Inside DOM-Loaded event
    $(".soft-skills")
        // Bind events to your handlers
        .bind("enterviewport", showChart)
        .bind("leaveviewport", removeChart)

        // Initialize the plug-in
        .bullseye();
}

function renderRadialChart(options, type) {
    let data = $(`#${type}`).attr("data-percent");
    let label = $(`#${type}`).attr("data-label") || "";

    options.series = [`${data}`];
    options.labels = [`${label}`];
    let chart = new ApexCharts(document.querySelector(`#${type}`), options);
    chart.render();
}


window.addEventListener(
    "load",
    function () {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        const forms = document.getElementsByClassName("form");
        // Loop over them and prevent submission
        const validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener(
                "submit",
                function (event) {
                    event.preventDefault();
                    event.stopPropagation();
                    if (!form.checkValidity()) {
                        form.classList.add("was-validated");
                        return;
                    }

                    const firstName = $("form #name").val();
                    const subject = $("form #subject").val();
                    const email = $("form #email").val();
                    const message = $("form #message").val();
                    $(
                        '#form-submit-btn'
                    ).attr('disabled', 'disabled');
                    $(
                        '#form-submit-btn'
                    ).text('Sending Your Message...');
                    submitData("https://warm-coast-82540.herokuapp.com/contact/me", {
                        firstName,
                        subject,
                        email,
                        message
                    }).then(data => {
                        $(
                            '#form-submit-btn'
                        ).attr('disabled', false);
                        $(
                            '#form-submit-btn'
                        ).text('Send Message');
                        if (data.status === "success") {
                            // Display a success toast, with a title
                            toastr.ingo('Your message is sent and delivered!', 'Success!')

                            $("form #firstName").val("");
                            $("form #email").val("");
                            $("form #subject").val("");
                            $("form #message").val("");
                        }
                    });
                },
                false
            );
        });
    },
    false
);

const submitData = async function (url, data) {
    const response = await fetch(url, {
        method: "POST", // *GET, POST, PUT, DELETE, etc.
        mode: "cors", // no-cors, *cors, same-origin
        cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
        credentials: "same-origin", // include, *same-origin, omit
        headers: {
            "Content-Type": "application/json"
            // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: "follow", // manual, *follow, error
        referrerPolicy: "no-referrer", // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        body: JSON.stringify(data) // body data type must match "Content-Type" header
    });

    return response.json(); // parses JSON response into native JavaScript objects
};