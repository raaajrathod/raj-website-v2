const express = require('express');

const contactController = require('./../controllers/contactController');

const router = express.Router({
  mergeParams: true
});

router.post('/me', contactController.contactMe);

module.exports = router;
