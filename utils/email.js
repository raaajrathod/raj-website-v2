const nodemailer = require('nodemailer');
const sgMail = require('@sendgrid/mail');
const pug = require('pug');
const htmlToText = require('html-to-text');

module.exports = class Email {
  constructor(user, url) {
    this.to = 'raaajrathod@gmail.com';
    this.firstName = user.firstName;
    this.url = url;
    this.from = `Admin <${process.env.EMAIL_FROM}>`;
    this.message = user.message;
    this.userEmail = user.email;
    this.lastName = user.lastName;
  }

  newTransport() {
    if (process.env.NODE_ENV === 'production') {
      // Send GRID
      // console.log('prod');
      return nodemailer.createTransport({
        host: 'smtp.sendgrid.net',
        port: '25',
        auth: {
          user: process.env.SENDGRID_USERNAME,
          pass: process.env.SENDGRID_PASSWORD
        }
      });
    }
    // console.log(process.env.NODE_ENV);
    return nodemailer.createTransport({
      host: process.env.EMAIL_HOST,
      port: process.env.EMAIL_PORT,
      auth: {
        user: process.env.EMAIL_USERNAME,
        pass: process.env.EMAIL_PASSWORD
      }

      // Activate in Gmail 'less secure app' option for Gmail
    });
  }

  async send(template, subject) {
    // Send Template
    //   Redner HTML
    sgMail.setApiKey(process.env.SENDGRID_PASSWORD);
    const html = pug.renderFile(`${__dirname}/../views/email/${template}.pug`, {
      firstName: this.firstName,
      lastName: this.lastName,
      message: this.message,
      userEmail: this.userEmail,
      url: this.url
    });
    // Define Email

    const mailOptions = {
      from: this.from,
      to: this.to,
      subject,
      html,
      text: htmlToText.fromString(html)
    };

    await this.newTransport().sendMail(mailOptions);
  }

  async sendContactMail() {
    await this.send('Hello', 'Contact Me');
  }
};
